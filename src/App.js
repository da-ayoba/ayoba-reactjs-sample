import React, { Component } from "react";
import './App.css';
import GetCountry from './GetCountry.js'

class App extends Component {
  render(){
  return (
    <div className="App">
      <header className="App-header">
      <React.StrictMode>
      <GetCountry></GetCountry>
      </React.StrictMode>
      </header>
    </div>
  );
}}

export default App;
